# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse
from shop.models import Category
from shop.models import Product

	
def product_list(request, catSlug=None):
	if catSlug == None:
		category = None
		products = Product.objects.filter().all
	else:
		category = Category.objects.get(catSlug = catSlug)	
		products = Product.objects.filter(category = category).all
	categories = Category.objects.filter().all	
	return render(request,'shop/list.html', {'category': category, 'categories': categories, 'products': products})

def product_detail(request, id, prodSlug):
	product = Product.objects.get(id = id)
	return render(request, 'shop/detail.html', {'product': product})
