# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.template.defaultfilters import slugify
from datetime import datetime 
from django.utils import timezone
from django.db.models.fields import DateTimeField

# Create your models here.


class Category(models.Model):
    catName = models.CharField(null = False, max_length=128, unique=True)
    catSlug = models.SlugField(unique=True)

    class Meta:
        verbose_name_plural='Categories'

    def save(self, *args,**kwargs):
        self.catSlug=slugify(self.catName)
        super(Category,self).save(*args,**kwargs)

    def __str__(self):
        return self.catName

class Product(models.Model):
    category = models.ForeignKey(Category, null = True)
    prodName = models.CharField(null = False, max_length=128, unique=True)
    prodSlug = models.SlugField(unique=True)
    image = models.ImageField(null = False, upload_to = 'images/products')
    description = models.CharField(null = False, max_length=1024)
    price = models.DecimalField(default = 0 ,max_digits=8, decimal_places=2, blank=True, null=False)
    stock = models.IntegerField(null = False, default = 1)
    availability = models.BooleanField(null = False, default = True)
    created = models.DateTimeField(default=timezone.now, blank=True)
    updated = models.DateTimeField(default=timezone.now, blank=True)

    class Meta:
        verbose_name_plural='Products'

    def save(self, *args,**kwargs):
        self.prodSlug=slugify(self.prodName)
        super(Product,self).save(*args,**kwargs)


    def __str__(self):
        return self.prodName

