Django==1.11
dj-static==0.0.6
dj-database_url==0.3.0
psycopg2==2.6.1
Pillow==2.8.2
static3==0.7.0
gunicorn==19.6.0
