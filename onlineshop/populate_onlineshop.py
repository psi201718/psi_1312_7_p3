import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'onlineshop.settings')

import django
django.setup()
from django.core.files import File
from shop.models import Category, Product
from django.conf import settings
import os.path as path


def populate():

	cocina = [
		{"prodName":"Batidora", "image":"batidora.png", "stock": 50, "price": 30.0, "description": "Batidora de cocina con varias velocidades",},
	{"prodName":"Congelador", "image":"congelador.jpg", "stock": 50, "price": 100.0, "description": "Congelador de cocina con cajones",},
	{"prodName":"Exprimidor", "image":"exprimidor.jpg", "stock": 50, "price": 20.0, "description": "Exprimidor de cocina automatico",},
	{"prodName":"Nevera", "image":"nevera.png", "stock": 50, "price": 400.0, "description": "Nevera de cocina con cajones",},
	{"prodName":"Tostadora", "image":"tostadora.png", "stock": 50, "price": 15.0, "description": "Tostadora de cocina con diferentes potencias",},
	{"prodName":"Vitroceramica", "image":"vitroceramica.png", "stock": 50, "price": 250.0, "description": "Vitroceramica electrica de cocina",} ]

	limpieza = [
		{"prodName":"Aspiradora", "image":"aspirador.png", "stock": 50, "price": 30.0, "description": "Aspirador de limpieza con varios niveles de potencia",},
	{"prodName":"Lavadora", "image":"lavadora.png", "stock": 50, "price": 200.0, "description": "Lavadora para la ropa con diferentes programas",},
	{"prodName":"Lavavajillas", "image":"lavavajillas.png", "stock": 50, "price": 350.0, "description": "Lavavajillas para platos con diferentes programas",},
	{"prodName":"Plancha", "image":"plancha.png", "stock": 50, "price": 70.0, "description": "Plancha de ropa con expulsion de vapor",},
	{"prodName":"Secadora", "image":"secadora.png", "stock": 50, "price": 200.0, "description": "Secadora de ropa ultrarapida",},
	{"prodName":"Trituradora", "image":"trituradora.jpeg", "stock": 50, "price": 500.0, "description": "Trituradora de papel de alta capacidad",} ]

	confort = [
		{"prodName":"Afeitadora", "image":"afeitadora.jpeg", "stock": 50, "price": 80.0, "description": "Afeitadora de barba y cabello",},
	{"prodName":"Calentador", "image":"calentador.jpg", "stock": 50, "price": 150.0, "description": "Calentador de agua electrico",},
	{"prodName":"Humidificador", "image":"humidificador.png", "stock": 50, "price": 45.0, "description": "Humidificador de aire con vapor",},
	{"prodName":"Manta termica", "image":"mantaelect.jpg", "stock": 50, "price": 25.0, "description": "Manta termica de calor con encendido rapido",},
	{"prodName":"Maquina coser", "image":"maquina_coser.jpg", "stock": 50, "price": 80.0, "description": "Maquina para coser ropa con distintas velocidades",},
	{"prodName":"Ventilador", "image":"ventilador.png", "stock": 50, "price": 40.0, "description": "Ventilador de aire con distintas velocidades",} ]


	cats = {"Cocina": {"products": cocina},
			"Limpieza": {"products": limpieza},
			"Confort": {"products": confort} }

	for cat, cat_data in cats.items():
		c = add_cat(cat)
		for p in cat_data["products"]:
			add_product(c, p["prodName"], p["image"], p["stock"], p["price"], p["description"])

	for c in Category.objects.all():
		for p in Product.objects.filter(category=c):
			print("- {0} - {1}".format(str(c), str(p)))

def add_product(cat, prodName, image_name, stock, price, description):

	p = Product.objects.get_or_create(category=cat, prodName=prodName, price=price, stock=stock, description=description)[0]
	imageObject = File(open(os.path.join("media/images/products", image_name),'r'))
	if not (path.exists("media/images/products"+image_name)):
		p.image.save(image_name, imageObject, save = True)
	p.save()
	return p

def add_cat(catName):
	c = Category.objects.get_or_create(catName=catName)[0]
	c.save()
	return c

# Start execution here!
if __name__ == '__main__':
	print("\nStarting Shop population script...")
	populate()
