import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'onlineshop.settings')

import django
django.setup()
from django.core.files import File
from shop.models import Category, Product
from django.conf import settings
import os.path as path


def add_product(cat, prodName, image_name, stock, price, description):

	p = Product.objects.get_or_create(category=cat, prodName=prodName, price=price, stock=stock, description=description)[0]
	imageObject = File(open(os.path.join(settings.BASE_DIR, "pictures", image_name)))
	if not (path.exists("media/images/products/"+image_name)):    
		p.image.save(image_name, imageObject, save = True)
	p.save()
	return p

def query():
	#Create ofertas category

	cat1 = Category.objects.get_or_create(catName="ofertas")[0]
	cat1.save()

	#Create gangas category

	cat2 = Category.objects.get_or_create(catName="gangas")[0]
	cat2.save()


	#Create oferta 1 product

	add_product(cat1, "oferta 1", "oferta1.png", 20, 20.0, "Primera oferta")

	#Create oferta 2 product

	add_product(cat1, "oferta 2", "oferta2.png", 20, 20.0, "Segunda oferta")

	#Create oferta 3 product

	add_product(cat1, "oferta 3", "oferta3.png", 20, 20.0, "Tercera oferta")

	#Create ganga 1 product

	add_product(cat2, "ganga 1", "ganga1.png", 25, 25.0, "Primera ganga")

	#Create ganga 2 product

	add_product(cat2, "ganga 2", "ganga2.png", 25, 25.0, "Segunda ganga")

	#Create ganga 3 product

	add_product(cat2, "ganga 3", "ganga3.png", 25, 25.0, "Tercera ganga")

	print(Category.objects.all())

	#Consulta 1

	gangas=Product.objects.filter(category__catName="gangas")
	print("\nListado de productos asociados a gangas: ")
	print gangas

	#Consulta 2

	p = Product.objects.get(prodSlug="oferta-1")
	print("\nListado de categorias con prodSlug oferta1: ")
	print(Category.objects.filter(catName = p.category))

	#Consulta 3

	nombre = "oferta-10"
	try:
		p = Product.objects.get(prodSlug=nombre)
	except Product.DoesNotExist:
		print("Producto " +nombre+ " inexistente")

# Start execution here!
if __name__ == '__main__':
	print("\nStarting Query OnlineShop script...")
	query()




